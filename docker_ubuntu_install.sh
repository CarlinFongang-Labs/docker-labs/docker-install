#!/bin/bash

## 1. Package update
apt list --upgradable
apt update -y


## 2. Installation of dependencies for Docker
apt install -y apt-transport-https ca-certificates curl software-properties-common

## 3. Adding the official Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

## 4. Adding the Docker repository to APT sources
echo "" | add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

## 5. Updating the package list
apt update -y

## 6. Docker installation
apt install -y docker-ce

## 7. Checking the status of Docker
#systemctl status docker

## 8. Adding the user to the Docker group
usermod -aG docker $USER
systemctl start docker
systemctl enable docker

## 9. Git installation
apt install -y git

echo "Docker et Git ont été installés avec succès."

# Compose
apt update && apt install -y curl gnupg2 lsb-release
curl -L https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update && apt install docker-compose - y
docker-compose --version
