#!/bin/bash

# Fonction pour vérifier si une commande existe
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

## 1. Vérification et installation de Docker
if command_exists docker; then
    echo "Docker est déjà installé."
else
    echo "Installation de Docker..."
    sudo amazon-linux-extras install -y docker
    sudo service docker start
    sudo usermod -a -G docker $USER
    sudo chkconfig docker on
    echo "Docker a été installé avec succès."
fi

## 2. Vérification et installation de Git
if command_exists git; then
    echo "Git est déjà installé."
else
    echo "Installation de Git..."
    sudo yum install -y git
    echo "Git a été installé avec succès."
fi

## 3. Vérification et installation de Docker Compose
if command_exists docker-compose; then
    echo "Docker Compose est déjà installé."
else
    echo "Installation de Docker Compose..."
    sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    echo "Docker Compose a été installé avec succès."
fi

# Affichage des versions installées
echo "Versions installées :"
docker --version
git --version
docker-compose --version

echo "Installation vérifiée et terminée."
sudo usermod -aG docker $USER
echo "user jenkins"
sudo visudo
jenkins ALL=(ALL) NOPASSWD: ALL
ec2-user ALL=(ALL) NOPASSWD: ALL
exit
