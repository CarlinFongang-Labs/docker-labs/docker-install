# To install the latest stable versions of Docker CLI, Docker Engine, Docker compose and git

[Please find the specifications by clicking](https://get.docker.com/)

# On centOS
````
## dependencies:
## 1. download the script
curl -fsSL https://get.docker.com -o install-docker.sh
````

````
## 2. verify the script's content
cat install-docker.sh
````

````
## 3. run the script with --dry-run to verify the steps it executes
sh install-docker.sh --dry-run
````

````
## 4. run the script either as root, or using sudo to perform the installation.
sudo sh install-docker.sh
````

````
## 5. Start services docker
systemctl start docker
systemctl enable docker
````

````
## 6. Add user on docker group
sudo usermod -aG docker $USER
````

````
## 7. Installation de Git
sudo apt install -y git
````

````
## 8. Docker compose
sudo yum update
sudo yum install docker-compose-plugin
````

````
echo "Docker et Git ont été installés avec succès."

````

![Alt text](image.png)


________________

# On ubuntu

````bash
#!/bin/bash

## 1. Package update
sudo apt list --upgradable
sudo apt update -y


## 2. Installation of dependencies for Docker
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

## 3. Adding the official Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

## 4. Adding the Docker repository to APT sources
echo "" | sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

## 5. Updating the package list
sudo apt update -y

## 6. Docker installation
sudo apt install -y docker-ce

## 7. Checking the status of Docker
#sudo systemctl status docker

## 8. Adding the user to the Docker group
sudo usermod -aG docker $USER
sudo systemctl start docker
sudo systemctl enable docker

## 9. Git installation
sudo apt install -y git

echo "Docker et Git ont été installés avec succès."
````

# Install Docker-compose

```bash
sudo apt update && sudo apt install -y curl gnupg2 lsb-release
curl -L https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update && sudo apt install docker-compose
docker-compose --version
```
ou

```bash
curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o docker-compose
chmod +x docker-compose
sudo mv docker-compose /usr/local/bin/
docker-compose --version
```



# On CentOS

````bash
#!/bin/bash

## 1. Package update
sudo yum check-update
sudo yum update -y

## 2. Installation of dependencies for Docker
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

## 3. Adding the official Docker repository
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

## 4. Installing Docker
sudo yum install -y docker-ce docker-ce-cli containerd.io

## 5. Starting and Enabling Docker
sudo systemctl start docker
sudo systemctl enable docker

## 6. Adding the user to the Docker group
sudo usermod -aG docker $USER

## 7. Git installation
sudo yum install -y git

echo "Docker and Git have been successfully installed."

````